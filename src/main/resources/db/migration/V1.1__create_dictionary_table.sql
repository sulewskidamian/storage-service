CREATE SEQUENCE product_status_id_seq START 1 INCREMENT BY 1;

CREATE TABLE IF NOT EXISTS dictionary.product_status
(
    id            BIGINT             NOT NULL
        CONSTRAINT product_status_pkey
            PRIMARY KEY,
    code          VARCHAR(20) UNIQUE NOT NULL,
    created_by    VARCHAR(60)        NOT NULL,
    created_date  TIMESTAMP          NOT NULL,
    modified_by   VARCHAR(60)        NOT NULL,
    modified_date TIMESTAMP          NOT NULL
);

CREATE SEQUENCE product_type_id_seq START 1 INCREMENT BY 1;

CREATE TABLE IF NOT EXISTS dictionary.product_type
(
    id            BIGINT             NOT NULL
        CONSTRAINT product_type_pkey
            PRIMARY KEY,
    code          VARCHAR(20) UNIQUE NOT NULL,
    created_by    VARCHAR(60)        NOT NULL,
    created_date  TIMESTAMP          NOT NULL,
    modified_by   VARCHAR(60)        NOT NULL,
    modified_date TIMESTAMP          NOT NULL
);