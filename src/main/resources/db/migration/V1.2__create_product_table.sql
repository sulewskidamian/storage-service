CREATE SEQUENCE product_id_seq START 1 INCREMENT BY 1;

CREATE TABLE IF NOT EXISTS product
(
    id                  BIGINT             NOT NULL
        CONSTRAINT product_pkey
            PRIMARY KEY,
    uuid                VARCHAR(60) UNIQUE NOT NULL,
    product_type_id     BIGINT             NOT NULL REFERENCES dictionary.product_type,
    product_status_id   BIGINT             NOT NULL REFERENCES dictionary.product_status,
    change_state_reason VARCHAR(255),
    add_date            TIMESTAMP          NOT NULL,
    last_work_date      TIMESTAMP          NOT NULL,
    last_break_date     TIMESTAMP,
    created_by          VARCHAR(60)        NOT NULL,
    created_date        TIMESTAMP          NOT NULL,
    modified_by         VARCHAR(60)        NOT NULL,
    modified_date       TIMESTAMP          NOT NULL
);

CREATE SEQUENCE parameter_id_seq START 1 INCREMENT BY 1;

CREATE TABLE IF NOT EXISTS parameter
(
    id            BIGINT       NOT NULL
        CONSTRAINT parameter_pkey
            PRIMARY KEY,
    product_id    BIGINT       NOT NULL REFERENCES product,
    parameter     VARCHAR(255) NOT NULL,
    value         VARCHAR(255) NOT NULL,
    created_by    VARCHAR(60)  NOT NULL,
    created_date  TIMESTAMP    NOT NULL,
    modified_by   VARCHAR(60)  NOT NULL,
    modified_date TIMESTAMP    NOT NULL
);
