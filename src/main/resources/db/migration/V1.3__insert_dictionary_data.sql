INSERT INTO dictionary.product_status (id, code, created_by, created_date, modified_by, modified_date)
VALUES (1, 'WORK', 'storage-service', now(), 'storage-service', now());
INSERT INTO dictionary.product_status (id, code, created_by, created_date, modified_by, modified_date)
VALUES (2, 'BREAK', 'storage-service', now(), 'storage-service', now());

INSERT INTO dictionary.product_type (id, code, created_by, created_date, modified_by, modified_date)
VALUES (1, 'TV', 'storage-service', now(), 'storage-service', now());
INSERT INTO dictionary.product_type (id, code, created_by, created_date, modified_by, modified_date)
VALUES (2, 'FRIDGE', 'storage-service', now(), 'storage-service', now());
INSERT INTO dictionary.product_type (id, code, created_by, created_date, modified_by, modified_date)
VALUES (3, 'WASHING_MACHINE', 'storage-service', now(), 'storage-service', now());
INSERT INTO dictionary.product_type (id, code, created_by, created_date, modified_by, modified_date)
VALUES (4, 'VACUUM_CLEANER', 'storage-service', now(), 'storage-service', now());