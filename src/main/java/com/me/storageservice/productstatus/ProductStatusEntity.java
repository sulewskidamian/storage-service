package com.me.storageservice.productstatus;

import com.me.storageservice.audit.AuditedEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Getter
@Setter
@Audited
@Entity
@NoArgsConstructor
@Table(schema = "dictionary", name = "product_status")
public class ProductStatusEntity extends AuditedEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "product_status_generator")
    @SequenceGenerator(name = "product_status_generator", sequenceName = "product_status_id_seq", allocationSize = 1)
    private Long id;

    private String code;
}
