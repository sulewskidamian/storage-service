package com.me.storageservice.productstatus;

public enum ProductStatus {
    WORK,
    BREAK
}
