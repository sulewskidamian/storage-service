package com.me.storageservice.productstatus;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProductStatusRepository extends JpaRepository<ProductStatusEntity, Long> {
    Optional<ProductStatusEntity> findByCode(String code);
}
