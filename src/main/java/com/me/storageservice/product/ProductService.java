package com.me.storageservice.product;

import com.me.storageservice.parameter.ParameterEntity;
import com.me.storageservice.parameter.ParameterRepository;
import com.me.storageservice.productstatus.ProductStatus;
import com.me.storageservice.productstatus.ProductStatusEntity;
import com.me.storageservice.productstatus.ProductStatusRepository;
import com.me.storageservice.producttype.ProductTypeEntity;
import com.me.storageservice.producttype.ProductTypeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;
    private final ParameterRepository parameterRepository;
    private final ProductTypeRepository productTypeRepository;
    private final ProductStatusRepository productStatusRepository;

    @Transactional
    public String createProduct(CreateProductRequest request) {
        ProductEntity productEntity = createProductEntity(
                productTypeRepository.findByCode(request.getProductType().toString())
                        .orElseThrow(() ->
                                new RuntimeException(String.format("ProductTypeEntity not found searching by code=[%s]",
                                        request.getProductType()))),
                productStatusRepository.findByCode(ProductStatus.WORK.toString())
                        .orElseThrow(() ->
                                new RuntimeException(String.format("ProductStatusEntity not found searching by code=[%s]",
                                        ProductStatus.WORK))));
        productRepository.save(productEntity);

        request.getProductParameters().forEach((parameter, value) -> {
            ParameterEntity parameterEntity = createParameterEntity(parameter, value, productEntity);
            parameterRepository.save(parameterEntity);
        });

        return productEntity.getUuid();
    }

    private ParameterEntity createParameterEntity(String parameter, String value, ProductEntity product) {
        return new ParameterEntity(
                parameter,
                value,
                product);
    }

    private ProductEntity createProductEntity(ProductTypeEntity productType, ProductStatusEntity userStatus) {
        return new ProductEntity(
                productType,
                userStatus);
    }

    @Transactional
    public void changeState(ChangeStageRequest request) {
        ProductEntity product = productRepository.findByUuid(request.getUuid())
                .orElseThrow(() ->
                        new RuntimeException(String.format("ProductEntity not found searching by uuid=[%s]",
                                request.getUuid())));
        product.setProductStatus(productStatusRepository.findByCode(request.getState().toString())
                .orElseThrow(() ->
                        new RuntimeException(String.format("ProductStatusEntity not found searching by code=[%s]",
                                request.getState()))));
        changeProductStateDate(request, product);
        product.setChangeStateReason(request.getChangeStateReason());
    }

    private void changeProductStateDate(ChangeStageRequest request, ProductEntity product) {
        if (ProductStatus.WORK.equals(request.getState())) {
            product.setLastWorkDate(LocalDateTime.now());
        } else if (ProductStatus.BREAK.equals(request.getState())) {
            product.setLastBreakDate(LocalDateTime.now());
        } else {
            throw new RuntimeException(String.format("State of product status not exists, state=[%s]",
                    request.getState()));
        }
    }
}
