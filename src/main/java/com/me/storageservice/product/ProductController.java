package com.me.storageservice.product;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("product")
public class ProductController {
    private final ProductService productService;

    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping("create")
    public String createProduct(@RequestBody CreateProductRequest request) {
        return productService.createProduct(request);
    }

    @ResponseStatus(value = HttpStatus.CREATED)
    @PutMapping("state")
    public void changeState(@RequestBody ChangeStageRequest request) {
        productService.changeState(request);
    }
}
