package com.me.storageservice.product;

import com.me.storageservice.producttype.ProductType;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;

@Getter
@AllArgsConstructor
public final class CreateProductRequest {
    private ProductType productType;
    private Map<String, String> productParameters;
}
