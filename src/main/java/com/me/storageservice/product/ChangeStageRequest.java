package com.me.storageservice.product;

import com.me.storageservice.productstatus.ProductStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public final class ChangeStageRequest {
    private String uuid;
    private ProductStatus state;
    private String changeStateReason;
}
