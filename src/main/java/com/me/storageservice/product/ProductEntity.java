package com.me.storageservice.product;

import com.me.storageservice.audit.AuditedEntity;
import com.me.storageservice.productstatus.ProductStatusEntity;
import com.me.storageservice.producttype.ProductTypeEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@Audited
@Entity
@NoArgsConstructor
@Table(name = "product")
public class ProductEntity extends AuditedEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "product_generator")
    @SequenceGenerator(name = "product_generator", sequenceName = "product_id_seq", allocationSize = 1)
    private Long id;

    private String uuid;
    private String changeStateReason;
    private LocalDateTime addDate;
    private LocalDateTime lastWorkDate;
    private LocalDateTime lastBreakDate;

    @ManyToOne
    private ProductTypeEntity productType;

    @ManyToOne
    private ProductStatusEntity productStatus;

    ProductEntity(ProductTypeEntity productType, ProductStatusEntity productStatus) {
        this.uuid = UUID.randomUUID().toString();
        this.productType = productType;
        this.productStatus = productStatus;
        this.addDate = LocalDateTime.now();
        this.lastWorkDate = LocalDateTime.now();
    }
}
