package com.me.storageservice.parameter;

import com.me.storageservice.audit.AuditedEntity;
import com.me.storageservice.product.ProductEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Getter
@Setter
@Audited
@Entity
@NoArgsConstructor
@Table(name = "parameter")
public class ParameterEntity extends AuditedEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "parameter_generator")
    @SequenceGenerator(name = "parameter_generator", sequenceName = "parameter_id_seq", allocationSize = 1)
    private Long id;

    private String parameter;
    private String value;

    @ManyToOne
    private ProductEntity product;

    public ParameterEntity(String parameter, String value, ProductEntity product) {
        this.parameter = parameter;
        this.value = value;
        this.product = product;
    }
}
