package com.me.storageservice.producttype;

public enum ProductType {
    TV,
    FRIDGE,
    WASHING_MACHINE,
    VACUUM_CLEANER
}
