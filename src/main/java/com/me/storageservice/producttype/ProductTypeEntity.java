package com.me.storageservice.producttype;

import com.me.storageservice.audit.AuditedEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Getter
@Setter
@Audited
@Entity
@NoArgsConstructor
@Table(schema = "dictionary", name = "product_type")
public class ProductTypeEntity extends AuditedEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "product_type_generator")
    @SequenceGenerator(name = "product_type_generator", sequenceName = "product_type_id_seq", allocationSize = 1)
    private Long id;

    private String code;
}
