package com.me.storageservice.producttype;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProductTypeRepository extends JpaRepository<ProductTypeEntity, Long> {
    Optional<ProductTypeEntity> findByCode(String code);
}
