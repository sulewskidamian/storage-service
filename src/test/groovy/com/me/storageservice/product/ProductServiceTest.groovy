package com.me.storageservice.product


import com.google.common.collect.ImmutableMap
import com.me.storageservice.parameter.ParameterRepository
import com.me.storageservice.productstatus.ProductStatus
import com.me.storageservice.productstatus.ProductStatusEntity
import com.me.storageservice.productstatus.ProductStatusRepository
import com.me.storageservice.producttype.ProductType
import com.me.storageservice.producttype.ProductTypeEntity
import com.me.storageservice.producttype.ProductTypeRepository
import spock.lang.Specification

class ProductServiceTest extends Specification {
    def productRepository = Mock(ProductRepository);
    def parameterRepository = Mock(ParameterRepository);
    def productTypeRepository = Mock(ProductTypeRepository);
    def productStatusRepository = Mock(ProductStatusRepository);

    def "create product should return product uuid"() {
        given:
        ProductService productService = new ProductService(productRepository, parameterRepository, productTypeRepository, productStatusRepository);
        CreateProductRequest request = new CreateProductRequest(ProductType.TV, ImmutableMap.of("Brand", "Sony", "Color", "Blue"))

        def productTypeEntity = new ProductTypeEntity();
        productTypeEntity.id = 1
        productTypeEntity.code = ProductType.TV.toString()
        productTypeRepository.findByCode(request.getProductType().toString()) >> Optional.of(productTypeEntity)

        def productStatusEntity = new ProductStatusEntity();
        productStatusEntity.id = 1
        productStatusEntity.code = ProductStatus.WORK.toString()
        productStatusRepository.findByCode(ProductStatus.WORK.toString()) >> Optional.of(productStatusEntity)

        expect:
        !productService.createProduct(request).isEmpty()
    }

    def "change state of product should change last work date if state is work"() {
        given:
        ProductService productService = new ProductService(productRepository, parameterRepository, productTypeRepository, productStatusRepository);
        ChangeStageRequest request = new ChangeStageRequest("0321-4124d3d-d2fsg", ProductStatus.WORK, "Because is dangerous")

        def productEntity = new ProductEntity();
        productEntity.id = 1
        productEntity.uuid = "0321-4124d3d-d2fsg"
        productEntity.changeStateReason = "Because is dangerous"
        productRepository.findByUuid(request.getUuid()) >> Optional.of(productEntity)

        def productStatusEntity = new ProductStatusEntity();
        productStatusEntity.id = 1
        productStatusEntity.code = ProductStatus.WORK.toString()
        productStatusRepository.findByCode(request.getState().toString()) >> Optional.of(productStatusEntity)

        when:
        productService.changeState(request)

        then:
        productEntity.getLastWorkDate() != null
        productEntity.getLastBreakDate() == null
    }

    def "change state of product should change last work date if state is break"() {
        given:
        ProductService productService = new ProductService(productRepository, parameterRepository, productTypeRepository, productStatusRepository);
        ChangeStageRequest request = new ChangeStageRequest("0321-4124d3d-d2fsg", ProductStatus.BREAK, "Because is dangerous")

        def productEntity = new ProductEntity();
        productEntity.id = 1
        productEntity.uuid = "0321-4124d3d-d2fsg"
        productEntity.changeStateReason = "Because is dangerous"
        productRepository.findByUuid(request.getUuid()) >> Optional.of(productEntity)

        def productStatusEntity = new ProductStatusEntity();
        productStatusEntity.id = 1
        productStatusEntity.code = ProductStatus.BREAK.toString()
        productStatusRepository.findByCode(request.getState().toString()) >> Optional.of(productStatusEntity)

        when:
        productService.changeState(request)

        then:
        productEntity.getLastWorkDate() == null
        productEntity.getLastBreakDate() != null
    }
}
