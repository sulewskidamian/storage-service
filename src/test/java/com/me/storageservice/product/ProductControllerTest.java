package com.me.storageservice.product;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import com.me.storageservice.productstatus.ProductStatus;
import com.me.storageservice.producttype.ProductType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ProductControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnStatusIsCreatedWhenProductIsAdded() throws Exception {
        CreateProductRequest request = new CreateProductRequest(ProductType.TV, ImmutableMap.of("Brand", "Sony"));
        mockMvc.perform(MockMvcRequestBuilders.post("/product/create")
                .contentType(APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(request)))
                .andExpect(status().isCreated());
    }

    @Test
    public void shouldReturnStatusIsCreatedWhenStateIsChanged() throws Exception {
        CreateProductRequest createProductRequest = new CreateProductRequest(ProductType.TV, ImmutableMap.of("Brand", "Sony"));
        MockHttpServletResponse productCreateResponse = mockMvc.perform(MockMvcRequestBuilders.post("/product/create")
                .contentType(APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(createProductRequest)))
                .andExpect(status().isCreated())
                .andReturn().getResponse();

        ChangeStageRequest changeStageRequest = new ChangeStageRequest(productCreateResponse.getContentAsString(), ProductStatus.BREAK, "Because is dangerous");
        mockMvc.perform(put("/product/state")
                .contentType(APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(changeStageRequest)))
                .andExpect(status().isCreated());
    }

    @Test
    public void shouldReturnStatusCodeNotFoundWhenEndPointNotExists() throws Exception {
        mockMvc.perform(put("/endpoint/not/exists"))
                .andExpect(status().isNotFound());
    }
}
